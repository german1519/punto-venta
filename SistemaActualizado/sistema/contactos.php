<?php include_once "includes/header.php"; ?>
<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Contactos</h1>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="table">
					<thead class="thead-dark">
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th>Dependencia</th>
							<th>Telefono 1</th>
							<th>Telefono 2</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$data = json_decode(file_get_contents('http://localhost/myService/index.php'), true);
						if ($data > 0) {
							for( $i=0; $i<count( $data ); $i++){
						?>
								<tr>
									<td><?php echo $data[$i]['id']; ?></td>
									<td><?php echo $data[$i]['nombre']; ?></td>
									<td><?php echo $data[$i]['dependencia']; ?></td>
									<td><?php echo $data[$i]['telefono1']; ?></td>
									<td><?php echo $data[$i]['telefono2']; ?></td>
								</tr>
						<?php }
						} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- /.container-fluid -->
<?php include_once "includes/footer.php"; ?>
